package com.repost.insta.adapter;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Environment;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.repost.insta.R;
import com.repost.insta.activity.ActivityRepostSave;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

/**
 * Created by Niyati on 2/14/2017.
 */

public class PostViewPagerAdapter extends PagerAdapter {

    Context context;
    List<String> photoUrl;
    List<String> type;
    List<String> video;
    int position;

    private ImageView imgRepostPhoto;
    private VideoView videoViewRepost;
    private FrameLayout layoutVideo;

    LayoutInflater mLayoutInflater;

    public PostViewPagerAdapter(Context context, List<String> photoUrl, List<String> type, List<String> video, int position) {
        this.context = context;
        this.photoUrl = photoUrl;
        this.type = type;
        this.video = video;
        this.position = position;
        mLayoutInflater = LayoutInflater.from(context);

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return photoUrl.size();
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.post_fragment, container, false);

        assert itemView != null;
        imgRepostPhoto = (ImageView) itemView.findViewById(R.id.imgRepostPhoto);
//        imgPlayBtn = (ImageView) findViewById(R.id.imgPlayBtn);
        layoutVideo = (FrameLayout) itemView.findViewById(R.id.layoutVideo);
        videoViewRepost = (VideoView) itemView.findViewById(R.id.videoViewRepost);

        setStatements();
        container.addView(itemView);

        return itemView;
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    private void setStatements() {
      /*  if (type.get(position).equals("video")) {
            layoutVideo.setVisibility(View.VISIBLE);
            imgRepostPhoto.setVisibility(View.GONE);
            MediaController mediaController = new MediaController(context);
            mediaController.setAnchorView(videoViewRepost);
            videoViewRepost.setMediaController(mediaController);
            videoViewRepost.setVideoPath(video.get(position));
            videoViewRepost.requestFocus();
            videoViewRepost.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    videoViewRepost.start();
                }
            });
        } else {*/
            layoutVideo.setVisibility(View.GONE);
            imgRepostPhoto.setVisibility(View.VISIBLE);

        System.out.println("Position image ::: " + photoUrl.get(position));
            Glide.with(context)
                    .load(photoUrl.get(position)) //this is optional the image to display while the url image is downloading
                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                    .into(imgRepostPhoto);
//        }
        final File outputVideo = new File(Environment.getExternalStorageDirectory().toString() + "/" + context.getString(R.string.app_name) + "/Videos");
        if (!outputVideo.exists())
            outputVideo.mkdir();
    }
}
